<!DOCTYPE html>
<html>
<head>
    <title>WORKSHOP LARAVEL 8 - http://www.sciencom.com/</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    @yield('content')
</div>

</body>
</html>
